extends Node2D

const IDLE_DURATION = 1
export var move = Vector2.UP * 1000
export var speed = 3

var follow = Vector2.ZERO

onready var platform = $KinematicBody2D
onready var tween = $Tween

func _ready():
    _init_tween()
    
func _init_tween():
   
    var duration = move.length() / float(speed * Lib.UNIT_SIZE)
    tween.interpolate_property(self, "follow", Vector2.ZERO, move, duration, tween.TRANS_LINEAR, tween.EASE_IN_OUT, IDLE_DURATION)
    tween.interpolate_property(self, "follow", move, Vector2.ZERO, duration, tween.TRANS_LINEAR, tween.EASE_IN_OUT, duration + IDLE_DURATION * 2)
    tween.set_repeat(true)
    tween.start()
    
func _physics_process(delta):
    platform.position = platform.position.linear_interpolate(follow, 0.075)
    
    
     
