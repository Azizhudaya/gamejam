extends Sprite

func _ready():
    var flare = get_node("anim")
    flare.play("fade_out")
    yield(flare, "animation_finished") 
    queue_free()
