extends Area2D
onready var parent = get_parent()

var DamageArea = Lib.DamageArea

func _physics_process(delta):
    var areas = get_overlapping_areas()

func _on_Hitbox_area_exited(area):
    if area is DamageArea:
        parent.health -= area.damage
        
