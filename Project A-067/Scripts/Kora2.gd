extends KinematicBody2D



export (float) var max_health = 3
onready var health = max_health setget _set_health

export (int) var speed = 500
export (int) var GRAVITY = 2000
export (int) var DAMAGE = 15
export (int) var HEALTH = 15

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var sprite = get_node("AnimatedSprite")

func _physics_process(delta):
    
    sprite.play("walk")
    
    velocity.y += delta * GRAVITY
    velocity = move_and_slide(velocity, UP)
    if is_on_wall():
        velocity.x = -speed
        speed = -speed
        if speed > 0:
            sprite.set_scale(Vector2(-2.5,2.5))
        else:
            sprite.set_scale(Vector2(2.5,2.5))
    else:
        velocity.x = speed
        
func _set_health(value):
    var prev_health = health
    health = clamp(value, 0, max_health)
    if health != prev_health:
        if health == 0:
            kill()
            
func kill(): 
    queue_free()
    
func damage(amount):
    _set_health(health - amount) 
