extends Area2D

export (float) var damage = 7.0

func _on_Ranjau_body_entered(body):
    if body.name == "Main character":
        
        body.health -= damage
