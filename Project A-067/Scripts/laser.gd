#script laser
extends Area2D

export var velocity = Vector2()
const scn_flare = preload('res://Scenes/flare.tscn')

func _ready():
    $AudioStreamPlayer2D.play()
    set_process(true)
    create_flare()
    if not get_node("VisibilityNotifier2D").is_on_screen():
        queue_free()
    
    
func _process(delta):
    translate(velocity * delta)
	
func create_flare():
    var flare = scn_flare.instance()
    flare.set_position(get_position())
    call_deferred("add_child", flare)
    

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
