extends KinematicBody2D

signal health_updated(update)
signal killed()

export (float) var max_health = 30
export (int) var speed = 750
export (int) var GRAVITY = 1500
export (int) var jump_speed = -1000

const UP = Vector2(0,-1)

var velocity = Vector2()
var double_jump = true
var is_vulnerable = true

onready var sprite = get_node("AnimatedSprite")
onready var health = max_health setget _set_health

var laser = preload("res://Scenes/Laser v2.tscn")
var gun
var grab_gem = false

func _ready():
    $AudioStreamPlayer2D.play()
    
func _physics_process(delta):
    velocity.y += delta * GRAVITY
    velocity = move_and_slide(velocity, UP)

func _input(event):    
    var direction = 0
    var just_pressed = event.is_pressed() and not event.is_echo()
    var pos_mouse_x = get_global_mouse_position().x
    var pos_x = position.x
    
    velocity.x = 0
    
    #input movement
    if Input.is_key_pressed(KEY_W) and just_pressed:
        direction = 0
        if is_on_floor() == false and double_jump:
            velocity.y = jump_speed
            double_jump = false
        elif is_on_floor() : 
            velocity.y = jump_speed
            double_jump = true
            
    if Input.is_key_pressed(KEY_D):
        velocity.x += speed
        direction = 1
        
    elif Input.is_key_pressed(KEY_A):
        velocity.x -= speed
        direction = 1
        
    elif Input.is_key_pressed(KEY_S):
        velocity.y += speed
        direction = 0
    #-------------------------------------------------------------
    #Shoot
    if event.is_action_pressed("shoot"):
        shoot()
	#------------------------------------------------------------- 
    #Animation
    if is_on_floor(): 
        if(direction == 0) :
            sprite.play("idle")
        elif(abs(direction) == 1):
            sprite.play("walk")
    else:
        sprite.play("jump")
        
    if not is_vulnerable:
        sprite.play("hurt")
    
    #--------------------------------------------------------------
    #Sprite
    if pos_mouse_x - pos_x > 0 :
        sprite.set_scale(Vector2(1,1))
        gun = $gun1
    else:
        sprite.set_scale(Vector2(-1,1))
        gun = $gun2
        
    if grab_gem:
        var objective = get_tree().get_root().get_node("Level1/CanvasLayer/objectives")
        objective.set_text_objectives()
        
func shoot():
    var rotation = get_global_mouse_position() - gun.global_position
    
    var b = laser.instance()
    b.start(gun.global_position, rotation.angle())
    get_parent().add_child(b)
    
func _set_health(value):
    if is_vulnerable:
        var healthbar = get_tree().get_root().get_node("Level1/CanvasLayer/HealthBar")
        var prev_health = health
        health = clamp(value, 0, max_health)
        is_vulnerable = false
        $Timer.start()
        if health != prev_health:
            healthbar.health_update(value)
            if health == 0:
                kill()
 
            
func kill():
    get_tree().change_scene(str("res://Scenes/lose.tscn"))
    
func damage(amount):
    _set_health(health - amount)
    
            
func _on_Timer_timeout():
    is_vulnerable = true
