extends Node2D

onready var health_bar = $HealthBar

func health_update(health):
    health_bar.value = health

func max_health_updated(max_health):
    health_bar.max_value = max_health    