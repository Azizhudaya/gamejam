extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)

func _process(delta):
    var pos_mouse = get_global_mouse_position()
    set_global_position(pos_mouse)
	